from timeit import timeit
from inference.bloom7b1 import Bloom7b1

def main(): 
	print("Starting Bloom7b1 Inference Benchmark")
	print("Instantiating Model...")
	print("this can take a few minutes...")
	model = Bloom7b1()
	print("Model Instantiated")
	print("Starting Main Benchmark...")
	bench = timeit(lambda: model.inference(), number=1)
	print("Bloom7b1 Inference Benchmark completed in: " + str(bench))


if __name__ == "__main__": 
	main()