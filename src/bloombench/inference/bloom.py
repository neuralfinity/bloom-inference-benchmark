import transformers
from transformers import BloomForCausalLM
from transformers import BloomTokenizerFast
import torch

class Bloom:
    def __init__(self):
        self.model = BloomForCausalLM.from_pretrained("bigscience/bloom")
        self.tokenizer = BloomTokenizerFast.from_pretrained("bigscience/bloom")
        # Benchmark Prompt


    def inference(self):
        prompt = "A few nights ago, a supermoon rose over the city."
        result_length = 120
        inputs = self.tokenizer(prompt, return_tensors="pt")
        print(self.tokenizer.decode(self.model.generate(inputs["input_ids"],
                       max_length=result_length, 
                       num_beams=2, 
                       no_repeat_ngram_size=2,
                       early_stopping=True
                      )[0]))