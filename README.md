# Bloom Inference Benchmark (Bloombench)

A Bloom Inference Benchmark

## Installation
If you do not have Pytorch installed, please run install.sh to install Pytorch and other dependencies.

Otherwise, just running `pip install -r requirements.txt` should be enough.
It is recommended to use a virtual environment.